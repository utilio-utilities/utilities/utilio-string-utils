const { UndefinedStringError } = require('../exceptions');

const removeSpecialCharsRegex = new RegExp(/[\u0300-\u036f]/, 'gm');
const emptySpacesRegex = new RegExp(/ /, 'gm');
const twoOrMoreEmptySpacesRegex = new RegExp(/ +/, 'gm');
const normalizationMethod = 'NFD';

/**
 * normalize
 * Normalize string by removing accents and special characters (ç, ã, ...)
 *
 * @name normalize
 * @function
 * @param {String} str The string to be normalized
 * @returns {String} The normalized version of the string
 */
exports.normalize = (si) => {
  if (si === undefined || si === null) throw new UndefinedStringError();

  return si.normalize(normalizationMethod).replace(removeSpecialCharsRegex, '').toString();
};

/**
 * removeEmptySpacesFromString
 * Remove all spaces from string
 *
 * @name removeEmptySpacesFromString
 * @function
 * @param {String} str The string to remove empty spaces from
 * @returns {String} The string without empty spaces
 */
exports.removeEmptySpacesFromString = (si) => {
  if (si === undefined || si === null) throw new UndefinedStringError();

  return si.replace(emptySpacesRegex, '');
};

/**
 * removeDuplicatedEmptySpacesFromString
 * Remove all duplicated spaces from string
 *
 * @name removeDuplicatedEmptySpacesFromString
 * @function
 * @param {String} str The string to remove duplicated empty spaces from
 * @returns {String} The string without duplicated empty spaces
 */
exports.removeDuplicatedEmptySpacesFromString = (si) => {
  if (si === undefined || si === null) throw new UndefinedStringError();

  return si.replace(twoOrMoreEmptySpacesRegex, ' ');
};
