const { UndefinedStringError } = require('../exceptions');

/**
 * diceCoefficient
 * Calculate string similarity using diceCoefficient algorithm
 *
 * @name diceCoefficient
 * @function
 * @param {String} si1 First string
 * @param {String} si2 Second string
 * @param {Boolean} ignoreCase Ignore case when finding similarity (default=false)
 * @returns {Float} Percentage of similarity (0 to 1)
 */
exports.diceCoefficient = (si1, si2, ignoreCase = false) => {
  // Check if any string is null or undefined
  if (si1 === undefined || si1 === null
    || si2 === undefined || si2 === null) throw new UndefinedStringError();

  const si1Lower = ignoreCase ? si1.toLowerCase() : si1;
  const si2Lower = ignoreCase ? si2.toLowerCase() : si2;

  const si1Length = si1.length - 1;
  const si2Length = si2.length - 1;
  if (si1Length < 1 || si2Length < 1) return 0;


  const bigramsS2 = new Map();
  let i;

  for (i = 0; i < si2Length; i += 1) {
    const bigram = si2Lower.substr(i, 2);
    const count = bigramsS2.has(bigram)
      ? bigramsS2.get(bigram) + 1
      : 1;
    bigramsS2.set(bigram, count);
  }

  let intersection = 0;
  for (i = 0; i < si1Length; i += 1) {
    const bigram = si1Lower.substr(i, 2);
    const count = bigramsS2.has(bigram)
      ? bigramsS2.get(bigram)
      : 0;
    if (count > 0) {
      bigramsS2.set(bigram, count - 1);
      intersection += 1;
    }
  }

  const total = si1Length + si2Length;
  return (intersection * 2.0) / total;
};


/**
 * levenshtein
 * Calculate string similarity using levenshtein algorithm
 *
 * @name levenshtein
 * @function
 * @param {String} first First string
 * @param {String} second Second string
 * @returns {Int} Similarity between strings
 */
exports.levenshtein = (first, second) => {
  // Check if any string is null or undefined
  if (first === undefined || first === null
    || second === undefined || second === null) throw new UndefinedStringError();

  // make it case insensitive
  const firstLower = first.toLowerCase();
  const secondLower = second.toLowerCase();
  const costs = [];

  // increment along the first column of each row
  let i;
  for (i = 0; i <= secondLower.length; i += 1) {
    costs[i] = [i];
  }

  // increment each column in the first row
  let j;
  for (j = 0; j <= firstLower.length; j += 1) {
    costs[0][j] = j;
  }

  let cost;
  // create costs array
  for (i = 1; i <= secondLower.length; i += 1) {
    const charAtI = secondLower.charAt(i - 1);

    for (j = 1; j <= firstLower.length; j += 1) {
      const chartAtJ = firstLower.charAt(j - 1);

      if (charAtI === chartAtJ) {
        cost = 0;
      } else {
        cost = 1;
      }

      costs[i][j] = Math.min(costs[i - 1][j] + 1,
        Math.min(costs[i][j - 1] + 1, costs[i - 1][j - 1] + cost));
    }
  }

  return costs[second.length][first.length];
};
