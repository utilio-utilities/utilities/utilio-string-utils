/* eslint-disable no-undef */
const { expect } = require('chai');
const differenceModule = require('../../analyser/difference');
const { UndefinedStringError } = require('../../exceptions');

let firstString;
let firstStringWithMoreSpacesBetweenWords;
let firstStringSmaller;
let firstTofirstSmallerDiff;

beforeEach('Create testing strings', () => {
  firstString = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Posuere sollicitudin aliquam ultrices sagittis orci a scelerisque purus. Amet est placerat in egestas erat imperdiet. Donec massa sapien faucibus et molestie ac. Arcu cursus vitae congue mauris.';
  firstStringWithMoreSpacesBetweenWords = 'Lorem ipsum     dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Posuere sollicitudin aliquam ultrices sagittis orci a scelerisque purus. Amet est placerat in egestas erat imperdiet. Donec massa sapien faucibus et molestie ac. Arcu cursus vitae congue mauris.';
  firstStringSmaller = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Posuere sollicitudin aliquam ultrices sagittis orci a scelerisque purus. Amet est placerat in egestas erat imperdiet. ';
  firstTofirstSmallerDiff = 'Donec massa sapien faucibus et molestie ac. Arcu cursus vitae congue mauris.';
});

describe('differenceModule', () => {
  describe('calcDifferences', () => {
    it('should throw undefined string exception', () => {
      expect(() => differenceModule.calcDifferences(undefined, 'undefined')).to.throw(UndefinedStringError);
      expect(() => differenceModule.calcDifferences('undefined', undefined)).to.throw(UndefinedStringError);
      expect(() => differenceModule.calcDifferences(undefined, undefined))
        .to.throw(UndefinedStringError);
    });
    it('should throw null string exception', () => {
      expect(() => differenceModule.calcDifferences(null, 'null')).to.throw(UndefinedStringError);
      expect(() => differenceModule.calcDifferences('null', null)).to.throw(UndefinedStringError);
      expect(() => differenceModule.calcDifferences(null, null)).to.throw(UndefinedStringError);
    });
    it('should not return null or undefined string exception', () => {
      expect(() => differenceModule.calcDifferences('null', 'null')).not.to.throw(UndefinedStringError);
    });
    it('should return no differences as result', () => {
      const result = differenceModule.calcDifferences(firstString, firstString);
      // eslint-disable-next-line no-unused-expressions
      expect(result).to.be.empty;
    });
    it('should return differences as result', () => {
      const result = differenceModule.calcDifferences(firstString, firstStringSmaller);
      // eslint-disable-next-line no-unused-expressions
      expect(result.length).to.equal(1);
      expect(result[0].firstStringValue).to.equal(firstTofirstSmallerDiff);
    });
    it('should return firstString as result', () => {
      const result = differenceModule.calcDifferences(firstString, '');
      // eslint-disable-next-line no-unused-expressions
      expect(result.length).to.equal(1);
      expect(result[0].firstStringValue).to.equal(firstString);
    });
    it('should return secondString as result', () => {
      const result = differenceModule.calcDifferences('    ', firstString);
      // eslint-disable-next-line no-unused-expressions
      expect(result.length).to.equal(1);
      expect(result[0].secondStringValue).to.equal(firstString);
    });

    it('should return different strings as result', () => {
      const result = differenceModule.calcDifferences(
        firstStringWithMoreSpacesBetweenWords, firstString,
      );
      // eslint-disable-next-line no-unused-expressions
      expect(result.length).to.be.greaterThan(0);
      expect(result[0].secondStringValue).to.equal('dolor');
      expect(result[0].firstStringValue).to.equal('');
    });
  });
});
