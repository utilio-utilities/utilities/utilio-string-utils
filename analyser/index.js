const { levenshtein, diceCoefficient } = require('./distance');
const { calcDifferences } = require('./difference');
const { duplicateCharacters, duplicateSentences, duplicateWords } = require('./duplicates');

module.exports = {
  distance: {
    levenshtein,
    diceCoefficient,
  },
  calcDifferences,
  duplicates: {
    duplicateCharacters,
    duplicateSentences,
    duplicateWords,
  },
};
