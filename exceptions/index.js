const UndefinedStringError = require('./UndefinedStringError');

module.exports = {
  UndefinedStringError,
};
