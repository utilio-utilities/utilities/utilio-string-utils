const { UndefinedStringError } = require('../exceptions');

const spaceSplitRegex = new RegExp(/ /, 'gm');

/**
 * calcDifferences
 * Calculate difference between two string
 *
 * @name calcDifferences
 * @function
 * @param {String} first String to be compared to string "second"
 * @param {String} second String to be compared to string "first"
 * @returns {List} List with all differences between "first" and "second" and its position.
 */
exports.calcDifferences = (first, second) => {
  if (first === undefined || first === null
     || second === undefined || second === null) throw new UndefinedStringError();

  if (first.trim() === '' || second.trim() === '') {
    return [{
      index: 0,
      firstStringValue: first,
      secondStringValue: second,
    }];
  }

  const firstSplitted = first.trim().split(spaceSplitRegex);
  const secondSplitted = second.trim().split(spaceSplitRegex);

  let counter = 0;
  const result = [];

  const secondStringLength = secondSplitted.length;
  const firstStringLength = firstSplitted.length;
  const minLength = secondStringLength < firstStringLength ? secondStringLength : firstStringLength;

  while (counter < minLength) {
    if (firstSplitted[counter] !== secondSplitted[counter]) {
      result.push({
        index: counter,
        firstStringValue: firstSplitted[counter],
        secondStringValue: secondSplitted[counter],
      });
    }
    counter += 1;
  }


  if (secondStringLength < firstStringLength) {
    result.push({
      index: counter,
      firstStringValue: firstSplitted.slice(counter, firstSplitted.length).join(' '),
      secondStringValue: '',
    });
  } else if (firstStringLength > secondStringLength) {
    result.push({
      index: counter,
      firstStringValue: '',
      secondStringValue: secondSplitted.slice(counter, secondSplitted.length).join(' '),
    });
  }


  return result;
};
