/* eslint-disable no-undef */
const { expect } = require('chai');
const normalizerModule = require('../../converters/normalizer');
const { UndefinedStringError } = require('../../exceptions');

let testingString;
let testingStringNormalized;
let testingStringNormalizedNoSpaces;

beforeEach('Create testing strings', () => {
  testingString = 'Lorem ipsum  çãoé  dolor sit amet,\n consectetur   adipiscing elit';
  testingStringNormalized = 'Lorem ipsum  caoe  dolor sit amet,\n consectetur   adipiscing elit';
  testingStringNormalizedNoSpaces = 'Loremipsumcaoedolorsitamet,\nconsecteturadipiscingelit';
  testingStringNormalizedOnlyOneSpace = 'Lorem ipsum caoe dolor sit amet,\n consectetur adipiscing elit';
});

describe('StringNormalizerConverterModule', () => {
  describe('normalize', () => {
    it('should throw undefined string exception', () => {
      expect(() => normalizerModule.normalize(undefined)).to.throw(UndefinedStringError);
      expect(() => normalizerModule.normalize(null))
        .to.throw(UndefinedStringError);
    });
    it('should return exactly the same string as input', () => {
      expect(normalizerModule.normalize(testingStringNormalized))
        .to.equal(testingStringNormalized);
    });
    it('should return the string version', () => {
      expect(normalizerModule.normalize(testingString))
        .to.equal(testingStringNormalized);
    });
  });

  describe('removeEmptySpacesFromString', () => {
    it('should throw undefined string exception', () => {
      expect(() => normalizerModule.removeEmptySpacesFromString(undefined))
        .to.throw(UndefinedStringError);
      expect(() => normalizerModule.removeEmptySpacesFromString(null))
        .to.throw(UndefinedStringError);
    });
    it('should return exactly the same string as input', () => {
      expect(normalizerModule.removeEmptySpacesFromString(testingStringNormalized))
        .to.equal(testingStringNormalizedNoSpaces);
    });
  });

  describe('removeDuplicatedEmptySpacesFromString', () => {
    it('should throw undefined string exception', () => {
      expect(() => normalizerModule.removeDuplicatedEmptySpacesFromString(undefined))
        .to.throw(UndefinedStringError);
      expect(() => normalizerModule.removeDuplicatedEmptySpacesFromString(null))
        .to.throw(UndefinedStringError);
    });
    it('should return exactly the same string as input', () => {
      expect(normalizerModule.removeDuplicatedEmptySpacesFromString(testingStringNormalized))
        .to.equal(testingStringNormalizedOnlyOneSpace);
    });
  });
});
