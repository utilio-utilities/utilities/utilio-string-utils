/* eslint-disable no-undef */
const assert = require('assert');
const stringLinesConverterModule = require('../../converters/lines');

let onlyLineString;
let twoParagraphsString;
let threeParagraphsString;
let threeParagraphsStringWithThreeEmptyStrings;

beforeEach('Create testing strings', () => {
  onlyLineString = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Posuere sollicitudin aliquam ultrices sagittis orci a scelerisque purus. Amet est placerat in egestas erat imperdiet. Donec massa sapien faucibus et molestie ac. Arcu cursus vitae congue mauris.';
  twoParagraphsString = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Posuere sollicitudin aliquam ultrices sagittis orci a scelerisque purus. Amet est placerat in egestas erat imperdiet. 
  Donec massa sapien faucibus et molestie ac. Arcu cursus vitae congue mauris.`;
  threeParagraphsString = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
  Posuere sollicitudin aliquam ultrices sagittis orci a scelerisque purus. Amet est placerat in egestas erat imperdiet. 
  Donec massa sapien faucibus et molestie ac. Arcu cursus vitae congue mauris.`;
  threeParagraphsStringWithThreeEmptyStrings = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

  
  Posuere sollicitudin aliquam ultrices sagittis orci a scelerisque purus. Amet est placerat in egestas erat imperdiet. 
  Donec massa sapien faucibus et molestie ac. Arcu cursus vitae congue mauris.
  `;
});

describe('StringLinesConverterModule', () => {
  describe('removeEmptyLines', () => {
    it('should return undefined string', () => {
      const str = stringLinesConverterModule.removeEmptyLines(undefined);
      assert.equal(str, undefined);
    });
    it('should return null string', () => {
      const str = stringLinesConverterModule.removeEmptyLines(null);
      assert.equal(str, null);
    });
    it('should return exactly same string', () => {
      let str = stringLinesConverterModule.removeEmptyLines(onlyLineString);
      assert.equal(str, onlyLineString);

      str = stringLinesConverterModule.removeEmptyLines(twoParagraphsString);
      assert.equal(str, twoParagraphsString);
    });
    it('should remove empty lines from string', () => {
      const str = stringLinesConverterModule
        .removeEmptyLines(threeParagraphsStringWithThreeEmptyStrings);
      assert.equal(str, threeParagraphsString);
    });
  });

  describe('toSingleLineString', () => {
    it('should return undefined string', () => {
      const str = stringLinesConverterModule.toSingleLineString(undefined);
      assert.equal(str, undefined);
    });
    it('should return null string', () => {
      const str = stringLinesConverterModule.toSingleLineString(null);
      assert.equal(str, null);
    });
    it('should return exactly same string', () => {
      const str = stringLinesConverterModule.toSingleLineString(onlyLineString);
      assert.equal(str, onlyLineString);
    });
    it('should return one line string', () => {
      let str = stringLinesConverterModule.toSingleLineString(twoParagraphsString);
      assert.equal(str, onlyLineString);

      str = stringLinesConverterModule
        .toSingleLineString(threeParagraphsStringWithThreeEmptyStrings);
      assert.equal(str, onlyLineString);
    });
  });
});
