/* eslint-disable no-undef */
const { expect } = require('chai');
const distanceModule = require('../../analyser/distance');
const { UndefinedStringError } = require('../../exceptions');

describe('distanceModule', () => {
  describe('levenshtein', () => {
    it('should throw undefined string exception', () => {
      expect(() => distanceModule.levenshtein(undefined, 'undefined')).to.throw(UndefinedStringError);
      expect(() => distanceModule.levenshtein('undefined', undefined)).to.throw(UndefinedStringError);
      expect(() => distanceModule.levenshtein(undefined, undefined))
        .to.throw(UndefinedStringError);
    });
    it('should throw null string exception', () => {
      expect(() => distanceModule.levenshtein(null, 'null')).to.throw(UndefinedStringError);
      expect(() => distanceModule.levenshtein('null', null)).to.throw(UndefinedStringError);
      expect(() => distanceModule.levenshtein(null, null)).to.throw(UndefinedStringError);
    });

    it('should return 0 difference when equal', () => {
      expect(distanceModule.levenshtein('', '')).to.equal(0);
      expect(distanceModule.levenshtein('yes', 'yes')).to.equal(0);
      expect(distanceModule.levenshtein('yes sir', 'yes sir')).to.equal(0);
    });

    it('should return non 0 difference', () => {
      expect(distanceModule.levenshtein('mississippi', 'swiss miss')).to.equal(8);
      expect(distanceModule.levenshtein('stop', 'tops')).to.equal(2);
      expect(distanceModule.levenshtein('abc', '')).to.equal(3);
    });
  });

  describe('diceCoefficient', () => {
    it('should throw undefined string exception', () => {
      expect(() => distanceModule.diceCoefficient(undefined, 'undefined')).to.throw(UndefinedStringError);
      expect(() => distanceModule.diceCoefficient('undefined', undefined)).to.throw(UndefinedStringError);
      expect(() => distanceModule.diceCoefficient(undefined, undefined))
        .to.throw(UndefinedStringError);
    });
    it('should throw null string exception', () => {
      expect(() => distanceModule.diceCoefficient(null, 'null')).to.throw(UndefinedStringError);
      expect(() => distanceModule.diceCoefficient('null', null)).to.throw(UndefinedStringError);
      expect(() => distanceModule.diceCoefficient(null, null)).to.throw(UndefinedStringError);
    });
    it('should return 1 difference when equal', () => {
      expect(distanceModule.diceCoefficient('yes', 'yes')).to.equal(1);
      expect(distanceModule.diceCoefficient('yes sir', 'yes sir')).to.equal(1);
      expect(distanceModule.diceCoefficient('yes sir', 'yes SiR', true)).to.equal(1);
    });
    it('should return not return 1 equality when case sensitive', () => {
      expect(distanceModule.diceCoefficient('yes sir', 'YAs SiR')).to.be.lessThan(1);
      expect(distanceModule.diceCoefficient('yes sir', 'YEs SiR')).to.be.lessThan(1);
    });
    it('should return 0 difference when string is empty', () => {
      expect(distanceModule.diceCoefficient('', 'yes')).to.equal(0);
      expect(distanceModule.diceCoefficient('yes', '')).to.equal(0);
      expect(distanceModule.diceCoefficient('', '')).to.equal(0);
    });
    it('should return string difference different from 1', () => {
      expect(distanceModule.diceCoefficient('night', 'nacht')).to.equal(0.25);
      expect(distanceModule.diceCoefficient('NiGHt', 'nacht', true)).to.equal(0.25);
      expect(distanceModule.diceCoefficient('stop', 'tops')).to.be.lessThan(1);
      expect(distanceModule.diceCoefficient('abc', 'abb')).to.be.lessThan(1);
    });
  });
});
