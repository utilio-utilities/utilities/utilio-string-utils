/* eslint-disable no-undef */
const { expect } = require('chai');
const duplicates = require('../../analyser/duplicates');
const { UndefinedStringError } = require('../../exceptions');

// let testStringWithDuplicates;
let testStringWithoutDuplicates;

beforeEach('Create testing strings', () => {
  testStringWithDuplicates = 'Lorem ipsum dolor testa-te sit amet, amet dolor, testa-te.\n Lorem ipsum dolor testa-te sit amet, amet dolor, testa-te.';
  testStringWithoutDuplicates = 'Lorem ipsum dolor sit amet';
});

describe('duplicatesModule', () => {
  describe('duplicateWords', () => {
    it('should throw undefined string exception', () => {
      expect(() => duplicates.duplicateWords(undefined)).to.throw(UndefinedStringError);
    });
    it('should throw null string exception', () => {
      expect(() => duplicates.duplicateWords(null)).to.throw(UndefinedStringError);
    });
    it('should return no duplicates', () => {
      const duplicateWords = duplicates.duplicateWords(testStringWithoutDuplicates);
      // eslint-disable-next-line no-unused-expressions
      expect(duplicateWords).to.be.empty;
    });
    it('should return duplicates', () => {
      const duplicateWords = duplicates.duplicateWords(testStringWithDuplicates);
      // eslint-disable-next-line no-unused-expressions
      expect(duplicateWords).not.to.be.empty;
      expect(duplicateWords.get('dolor')).to.equal(4);
      expect(duplicateWords.get('amet')).to.equal(4);
      expect(duplicateWords.get('testa-te')).to.equal(4);
    });
  });

  describe('duplicateSentences', () => {
    it('should throw undefined string exception', () => {
      expect(() => duplicates.duplicateSentences(undefined)).to.throw(UndefinedStringError);
    });
    it('should throw null string exception', () => {
      expect(() => duplicates.duplicateSentences(null)).to.throw(UndefinedStringError);
    });
    it('should return no duplicated sentences', () => {
      const duplicateSentences = duplicates.duplicateSentences(testStringWithoutDuplicates);
      // eslint-disable-next-line no-unused-expressions
      expect(duplicateSentences).to.be.empty;
    });
    it('should return duplicated sentences', () => {
      const duplicateSentences = duplicates.duplicateSentences(testStringWithDuplicates);
      // eslint-disable-next-line no-unused-expressions
      expect(duplicateSentences).not.to.be.empty;
      expect(duplicateSentences.get('Lorem ipsum dolor testa-te sit amet, amet dolor, testa-te')).to.equal(2);
    });
  });

  describe('duplicateCharacters', () => {
    it('should throw undefined string exception', () => {
      expect(() => duplicates.duplicateCharacters(undefined)).to.throw(UndefinedStringError);
    });
    it('should throw null string exception', () => {
      expect(() => duplicates.duplicateCharacters(null)).to.throw(UndefinedStringError);
    });
    it('should return duplicated sentences', () => {
      const duplicateCharacters = duplicates.duplicateCharacters(testStringWithDuplicates);

      // eslint-disable-next-line no-unused-expressions
      expect(duplicateCharacters).not.to.be.empty;
      expect(duplicateCharacters.get('L')).to.equal(2);
      expect(duplicateCharacters.get('a')).to.equal(8);
      expect(duplicateCharacters.get('t')).to.equal(18);
      expect(duplicateCharacters.get(',')).to.equal(4);
      expect(duplicateCharacters.get(' ')).to.equal(17);
      // eslint-disable-next-line no-unused-expressions
      expect(duplicateCharacters.get('\n')).to.be.undefined;
      // eslint-disable-next-line no-unused-expressions
      expect(duplicateCharacters.get('z')).to.be.undefined;
    });
  });
});
