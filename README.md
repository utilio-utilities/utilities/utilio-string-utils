# Utilio String Utils

Set of utilities to easily analyse and normalize strings.

It is divided into two modules:

1. Analysers - can be used to analyse strings
2. Converters - can be used to normalize strings

## Getting Started

### Installing

```
npm install utilio-string
```

## Modules

### Analysers

Currently contains the following functionalities:

1. Calculate distance between two strings (similarity) - two algorithms available
    1. Levenshtein
    2. Dice Coefficient
2. Find differences between two strings
3. Find duplicated values in string:
    1. Characters
    2. Words
    3. Sentences


### Converters

Currently contains the following functionalities:

1. Remove empty lines - remove blank lines from a string
2. Multi line to single line string
3. Normalize - Normalize string by removing accents and special characters (ç, ã, ...)
4. Remove all spaces from string
5. Remove all duplicated spaces from string


## Authors

* José Miguel Melo