const { removeEmptyLines, toSingleLineString } = require('./lines');
const { normalize, removeEmptySpacesFromString, removeDuplicatedEmptySpacesFromString } = require('./normalizer');

module.exports = {
  removeEmptyLines,
  toSingleLineString,
  normalize,
  removeEmptySpacesFromString,
  removeDuplicatedEmptySpacesFromString,
};
