
/**
 * UndefinedStringError
 * Exception class thrown when string is null or undefined
 *
 * @name UndefinedStringError
 * @class
 */
class UndefinedStringError extends Error {
  constructor() {
    super('Undefined or null string');
    this.name = 'UndefinedStringError';
  }
}

module.exports = UndefinedStringError;
