/**
 * Regex to validate empty lines
 * @name emptyLineRegex
 * @returns {Regex}
 */
const emptyLineRegex = new RegExp(/(^[ \t]*\n)/, 'gm');

/**
 * Regex to validate new lines
 * @name newLineRegex
 * @returns {Regex}
 */
const newLineRegex = new RegExp(/([ \t]*[ \n\r]+[ \t]*)/, 'gm');

/**
 * removeEmptyLines
 * Remove blank lines from a string.
 *
 * @name removeEmptyLines
 * @function
 * @param {String} str The string to remove empty lines from.
 * @returns {String} The string without empty lines
 */
exports.removeEmptyLines = (str) => {
  if (!str) { return str; }
  return str.replace(emptyLineRegex, '').trim();
};


/**
 * toSingleLineString
 * Convert string to single line string.
 *
 * @name toSingleLineString
 * @function
 * @param {String} str The string to be converted into a single line string.
 * @returns {String} Single line string with same content as initial.
 */
exports.toSingleLineString = (str) => {
  if (!str) { return str; }
  return str.replace(newLineRegex, ' ').trim();
};
