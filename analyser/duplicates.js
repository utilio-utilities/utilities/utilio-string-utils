const { UndefinedStringError } = require('../exceptions');

const wordsRegex = new RegExp(/[\w-]+/, 'gm');
const sentencesRegex = new RegExp(/[^.]+/, 'gm');
const charactersRegex = new RegExp(/[\w\W]/, 'gm');

/**
 * duplicatesInList
 * Find duplicated elements in list
 *
 * @name duplicatesInList
 * @function
 * @param {Array} list List of elements where to find duplicates
 * @returns {Map} Map with all duplicated elements and the number of occurences for each element
 */
const duplicatesInList = (list) => {
  const duplicates = list.reduce((accumulator, val) => {
    const key = val !== ' ' ? val.trim() : val;
    const count = accumulator.has(key) ? accumulator.get(key) + 1 : 1;
    accumulator.set(key, count);
    return accumulator;
  }, new Map());

  duplicates.forEach((v, k) => {
    if (v === 1) {
      duplicates.delete(k);
    }
  });
  return duplicates;
};

/**
 * duplicateWords
 * Find duplicated words in string
 *
 * @name duplicateWords
 * @function
 * @param {String} si String where to find duplicated words
 * @returns {Map} Map with all duplicated words and the number of occurences for each word
 */
exports.duplicateWords = (si) => {
  if (si === undefined || si === null) throw new UndefinedStringError();

  const words = si.match(wordsRegex);

  return duplicatesInList(words);
};

/**
 * duplicateSentences
 * Find duplicated sentences in string
 *
 * @name duplicateSentences
 * @function
 * @param {String} si String where to find duplicated sentences
 * @returns {Map} Map with all duplicated sentences and the number of occurences for each sentence
 */
exports.duplicateSentences = (si) => {
  if (si === undefined || si === null) throw new UndefinedStringError();

  const sentences = si.match(sentencesRegex);

  return duplicatesInList(sentences);
};

/**
 * duplicateCharacters
 * Find duplicated characters in string
 *
 * @name duplicateCharacters
 * @function
 * @param {String} si String where to find duplicated characters
 * @returns {Map} Map with all duplicated characters and the number of
 * occurences for each characters
 */
exports.duplicateCharacters = (si) => {
  if (si === undefined || si === null) throw new UndefinedStringError();

  const characters = si.match(charactersRegex);

  return duplicatesInList(characters);
};
